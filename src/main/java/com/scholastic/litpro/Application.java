package com.scholastic.litpro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application {

    private static final String LITPRO_API = "/litpro/api";

	@RequestMapping(LITPRO_API)
    public String home() {
        return "Welcome to Lit-pro-RESTful";
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
