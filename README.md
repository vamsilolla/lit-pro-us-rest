# Welcome to LitPRO-US-Rest


JDK 1.8 or later
Gradle 2.3+ or Maven 3.0+
Docker


Build application using Maven or Gradel


./gradlew build && java -jar build/libs/lit-pro-us-rest-0.1.0.jar

mvn package && java -jar target/lit-pro-us-rest-0.1.0.jar


Running the application. 

if you are using Eclipse.
Right click on the project and select Run As->Maven Clean

Right click on the project and select Run As->Maven Install

Right click on the project and select Run AS->Spring boot application.

 open browser http://localhost:8080/litpro/api



Docker Run


if you want to run app in Docker container you need Docker, which only runs on 64-bit machines. See https://docs.docker.com/installation/#installation for details on setting Docker up for your machine. Before proceeding further, verify you can run docker commands from the shell. If you are using boot2docker you need to run that first.

$ mvn package docker:build -DpushImage

$ docker ps

$ docker run -p 8080:8080 -t lit-pro-us-rest/litpro-rest